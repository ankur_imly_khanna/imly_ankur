//
//  SignUpViewController.h
//  Imly
//
//  Created by Ankur Khanna on 19/03/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController<UITextFieldDelegate>


@property(nonatomic,strong) UINavigationBar *navBar;

@property (strong, nonatomic) IBOutlet UITextField *email_text;

@property (strong, nonatomic) NSMutableDictionary *signUp_Dict;


- (IBAction)next_button_clicked:(id)sender;
- (IBAction)facebook_button_clicked:(id)sender;
- (IBAction)close:(id)sender;



@end
