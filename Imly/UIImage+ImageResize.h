//
//  UIImage+ImageResize.h
//  Imly
//
//  Created by Ankur Khanna on 27/03/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ImageResize)

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

@end
