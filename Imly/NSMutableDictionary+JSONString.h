//
//  NSMutableDictionary+JSONString.h
//  Imly
//
//  Created by Ankur Khanna on 03/04/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (JSONString)


-(NSString*) jsonStringWithPrettyPrint:(BOOL) prettyPrint;

@end
