//
//  DishDetailsController.m
//  Imly
//
//  Created by Ankur Khanna on 26/03/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import "DishDetailsController.h"


@interface DishDetailsController ()
{
    UIBarButtonItem *sell_button;
    
    NSNumber *yourNumber;               // holding the tag value of the button
    UIButton *button;
    
    NSMutableArray *first_array;    // Validation Arrays
    NSMutableArray *second_array;
    NSMutableArray *third_array;
    
    
   
    
    BOOL isFirstRow_firstFound;             // Logic helper for progress bar
    BOOL isSecondRow_firstFound;
    BOOL isThirdRow_firstFound;
    
    NSMutableArray *button_first_array;     // Array for holding button value acc to rows
    NSMutableArray *button_second_array;
    NSMutableArray *button_third_array;
    NSMutableArray *button_fourth_array;
    
    float x;                                // Variable for progress bar value.
    
    NSArray *main_array;                    // the main array holding the final values
    
        

    NSMutableDictionary *dic;               // To hold Hour and Minute logic in it
    int a;                                  // To hold global hour+minute in number
}

@end

@implementation DishDetailsController

@synthesize final_valueDic,progressView;

@synthesize veg,nonVeg,vegan,jain;
@synthesize appetizer,salad,mains,dessert;
@synthesize indian,asian,continental,other;
@synthesize dairyFree,nutFree,eggless,glutenFree;

@synthesize serve_centerLabel_text,valid_centerLabel_text,advance_centerLabel_text,price_centerLabel_text;

@synthesize price_left,price_right,timer;




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
         self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    isVegSelected = FALSE;
    isNonVegSelected = FALSE;
    isVeganSelected = FALSE;
    isJainSelected = FALSE;
    
    isAppetizerSelected = FALSE;
    isSaladSelected = FALSE;
    isMainsSelected = FALSE;
    isDessertSelected= FALSE;
    
    isIndianSelected = FALSE;
    isAsianSelected = FALSE;
    isContinentalSelected = FALSE;
    isOtherSelected = FALSE;
    
    isDairyFreeSelected = FALSE;
    isNutFreeSelected = FALSE;
    isEgglessSelected = FALSE;
    isGluttenFreeSelected = FALSE;
    
    isFirstRowValidated = FALSE;
    isSecondRowValidated = FALSE;
    isThirdRowValidated = FALSE;
    
    
    first_array = [NSMutableArray arrayWithObjects:@"0",@"1",@"2",@"3",nil];
    second_array = [NSMutableArray arrayWithObjects:@"4",@"5",@"6",@"7",nil];
    third_array = [NSMutableArray arrayWithObjects:@"8",@"9",@"10",@"11",nil];
    
    
    button_first_array = [[NSMutableArray alloc]init];
    button_second_array = [[NSMutableArray alloc]init];
    button_third_array = [[NSMutableArray alloc]init];
    button_fourth_array = [[NSMutableArray alloc]init];
    
    x=0.5f;
    
    main_array = [NSArray arrayWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"14",@"15", nil];
    
   // final_valueDic = [[NSMutableDictionary alloc]init];
    
    isFirstRow_firstFound = FALSE;
    isSecondRow_firstFound =FALSE;
    isThirdRow_firstFound = FALSE;
    
    price_centerLabel_text.text = @"200";
    serve_centerLabel_text.text = [@"1" stringByAppendingString:@" person"];
    valid_centerLabel_text.text = [@"4" stringByAppendingString:@" hour"];
    advance_centerLabel_text.text = [@"1" stringByAppendingString:@" hour"];
    
    [final_valueDic setValue:price_centerLabel_text.text forKey:@"price"];
    [final_valueDic setValue:serve_centerLabel_text.text forKey:@"serving"];
    [final_valueDic setValue:valid_centerLabel_text.text forKey:@"valid"];
    [final_valueDic setValue:advance_centerLabel_text.text forKey:@"advance"];
    
    dic = [[NSMutableDictionary alloc]init];
    a =60;
    
    
    UILongPressGestureRecognizer *leftLong = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(leftLongPress:)];
    [price_left addGestureRecognizer:leftLong];
    
    UILongPressGestureRecognizer *rightLong = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(rightLongPress:)];
    [price_right addGestureRecognizer:rightLong];
    
    
    
    
    
    self.tabBarController.tabBar.hidden = YES;
    self.navigationItem.title = @"Dish Details";
    
    sell_button = [[UIBarButtonItem alloc] initWithTitle:@"Sell" style:UIBarButtonItemStylePlain target:self action:@selector(sell)];
    sell_button.tintColor = [UIColor redColor];
    self.navigationItem.rightBarButtonItem = sell_button;
    
    sell_button.enabled = NO;
    
    NSLog(@"values in dict are %@",self.final_valueDic);
    
    // Do any additional setup after loading the view from its nib.
}


#pragma mark Bottom Button Logic

- (IBAction)button_clicked:(id)sender {
    
    button = (UIButton *)sender;
    yourNumber = [NSNumber  numberWithInteger:[sender tag]];
    
    switch ([sender tag]) {
        case 0:
            
            NSLog(@"veg");
        
                if(isVegSelected)
                {
                button.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                isVegSelected = FALSE;
                }
            
            else
            {
        
                
                veg.backgroundColor = [UIColor orangeColor];
                [veg setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                [button_first_array removeLastObject];
                [button_first_array addObject:yourNumber];
                
                isVegSelected = TRUE;
                
                isNonVegSelected = FALSE; isNonVegSelected = FALSE; isVeganSelected = FALSE; isJainSelected = FALSE; isAppetizerSelected = FALSE; isSaladSelected=FALSE; isMainsSelected=FALSE; isDessertSelected=FALSE; isIndianSelected=FALSE; isAsianSelected=FALSE; isContinentalSelected= FALSE; isOtherSelected=FALSE;
                isDairyFreeSelected = FALSE; isNutFreeSelected = FALSE; isEgglessSelected= FALSE;
                isGluttenFreeSelected = FALSE;
                
                [nonVeg setSelected:NO];
                nonVeg.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [nonVeg setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                [vegan setSelected:NO];
                vegan.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [vegan setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                [jain setSelected:NO];
                jain.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [jain setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
            }
            
            break;
         
        case 1:
            
            NSLog(@"non-veg");
            
            
            
            nonVeg.backgroundColor = [UIColor orangeColor];
            [nonVeg setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            

                if(isNonVegSelected)
                {
                
                    
                button.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                isNonVegSelected = FALSE;
                }
                else
                {
                    [button_first_array removeLastObject];
                    [button_first_array addObject:yourNumber];
                    
                    isNonVegSelected = TRUE;
                    
                    isVegSelected = FALSE; isNonVegSelected = FALSE; isVeganSelected = FALSE; isJainSelected = FALSE; isAppetizerSelected = FALSE; isSaladSelected=FALSE; isMainsSelected=FALSE; isDessertSelected=FALSE; isIndianSelected=FALSE; isAsianSelected=FALSE; isContinentalSelected= FALSE; isOtherSelected=FALSE;
                    isDairyFreeSelected = FALSE; isNutFreeSelected = FALSE; isEgglessSelected= FALSE;
                    isGluttenFreeSelected = FALSE;
                    
                    [veg setSelected:NO];
                    veg.backgroundColor = [UIColor groupTableViewBackgroundColor];
                    [veg setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                    
                    [vegan setSelected:NO];
                    vegan.backgroundColor = [UIColor groupTableViewBackgroundColor];
                    [vegan setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                    
                    [jain setSelected:NO];
                    jain.backgroundColor = [UIColor groupTableViewBackgroundColor];
                    [jain setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                    
                }
            
            
            break;
            
        case 2:
            NSLog(@"vegan");
            
            
            if(isVeganSelected)
            {
                
                button.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                isVeganSelected = FALSE;
                
            }
            else{
                
                vegan.backgroundColor = [UIColor orangeColor];
                [vegan setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                [button_first_array removeLastObject];
                [button_first_array addObject:yourNumber];
                
                isVeganSelected = TRUE;
                
                isNonVegSelected = FALSE; isNonVegSelected = FALSE; isVegSelected = FALSE; isJainSelected = FALSE; isAppetizerSelected = FALSE; isSaladSelected=FALSE; isMainsSelected=FALSE; isDessertSelected=FALSE; isIndianSelected=FALSE; isAsianSelected=FALSE; isContinentalSelected= FALSE; isOtherSelected=FALSE;
                isDairyFreeSelected = FALSE; isNutFreeSelected = FALSE; isEgglessSelected= FALSE;
                isGluttenFreeSelected = FALSE;
                
                [veg setSelected:NO];
                veg.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [veg setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                [nonVeg setSelected:NO];
                nonVeg.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [nonVeg setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                [jain setSelected:NO];
                jain.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [jain setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
            
            
            
            break;
            
        case 3:
            NSLog(@"jain");
            
            
            if(isJainSelected)
            {
        
                button.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                isJainSelected = FALSE;
            }
            else{
                
                jain.backgroundColor = [UIColor orangeColor];
                [jain setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                
                [button_first_array removeLastObject];
                [button_first_array addObject:yourNumber];
                
                isJainSelected = TRUE;
                
                isVegSelected=FALSE;   isNonVegSelected = FALSE; isVeganSelected = FALSE;isAppetizerSelected = FALSE; isSaladSelected=FALSE; isMainsSelected=FALSE; isDessertSelected=FALSE; isIndianSelected=FALSE; isAsianSelected=FALSE; isContinentalSelected= FALSE; isOtherSelected=FALSE;
                isDairyFreeSelected = FALSE; isNutFreeSelected = FALSE; isEgglessSelected= FALSE;
                isGluttenFreeSelected = FALSE;
                
                [veg setSelected:NO];
                veg.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [veg setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                [nonVeg setSelected:NO];
                nonVeg.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [nonVeg setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                [vegan setSelected:NO];
                vegan.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [vegan setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
            }
            
            break;
            
        case 4:
            NSLog(@"appetizer");
        
            if(isAppetizerSelected)
            {
                
                button.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                isAppetizerSelected = FALSE;
            }
            else{
                
                appetizer.backgroundColor = [UIColor orangeColor];
                [appetizer setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                [button_second_array removeLastObject];
                [button_second_array addObject:yourNumber];
                
                
                isAppetizerSelected = TRUE;
                
                isVegSelected=FALSE;   isNonVegSelected = FALSE; isVeganSelected = FALSE; isJainSelected=FALSE; isSaladSelected=FALSE; isMainsSelected=FALSE; isDessertSelected=FALSE; isIndianSelected=FALSE; isAsianSelected=FALSE; isContinentalSelected= FALSE; isOtherSelected=FALSE;
                isDairyFreeSelected = FALSE; isNutFreeSelected = FALSE; isEgglessSelected= FALSE;
                isGluttenFreeSelected = FALSE;
                
                [salad setSelected:NO];
                salad.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [salad setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                [mains setSelected:NO];
                mains.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [mains setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                [dessert setSelected:NO];
                dessert.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [dessert setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
                
            break;
            
        case 5:
            NSLog(@"salad");
             
                if(isSaladSelected)
                {
                    
                    button.backgroundColor = [UIColor groupTableViewBackgroundColor];
                    [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                    isSaladSelected = FALSE;
                }
                else{
                    
                    salad.backgroundColor = [UIColor orangeColor];
                    [salad setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    
                    [button_second_array removeLastObject];
                    [button_second_array addObject:yourNumber];
                   
                    
                    isSaladSelected = TRUE;
                    
                    isVegSelected=FALSE;   isNonVegSelected = FALSE; isVeganSelected = FALSE; isJainSelected=FALSE; isAppetizerSelected=FALSE; isMainsSelected=FALSE; isDessertSelected=FALSE; isIndianSelected=FALSE; isAsianSelected=FALSE; isContinentalSelected= FALSE; isOtherSelected=FALSE;
                    isDairyFreeSelected = FALSE; isNutFreeSelected = FALSE; isEgglessSelected= FALSE;
                    isGluttenFreeSelected = FALSE;
                    
                    [appetizer setSelected:NO];
                    appetizer.backgroundColor = [UIColor groupTableViewBackgroundColor];
                    [appetizer setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                    
                    [mains setSelected:NO];
                    mains.backgroundColor = [UIColor groupTableViewBackgroundColor];
                    [mains setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                    
                    [dessert setSelected:NO];
                    dessert.backgroundColor = [UIColor groupTableViewBackgroundColor];
                    [dessert setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            
                }
            break;
            
        case 6:
            NSLog(@"mains");
            
            
            if(isMainsSelected)
            {
                
                button.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                isMainsSelected = FALSE;
            }
            else{
                
                mains.backgroundColor = [UIColor orangeColor];
                [mains setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                [button_second_array removeLastObject];
                [button_second_array addObject:yourNumber];
               
                
                isMainsSelected = TRUE;
                
                isVegSelected=FALSE;   isNonVegSelected = FALSE; isVeganSelected = FALSE; isJainSelected=FALSE; isAppetizerSelected=FALSE; isSaladSelected=FALSE; isDessertSelected=FALSE; isIndianSelected=FALSE; isAsianSelected=FALSE; isContinentalSelected= FALSE; isOtherSelected=FALSE;
                isDairyFreeSelected = FALSE; isNutFreeSelected = FALSE; isEgglessSelected= FALSE;
                isGluttenFreeSelected = FALSE;
                
                [appetizer setSelected:NO];
                appetizer.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [appetizer setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                [salad setSelected:NO];
                salad.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [salad setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                [dessert setSelected:NO];
                dessert.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [dessert setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
            
            break;
            
        case 7:
            NSLog(@"dessert");
            
            
            if(isDessertSelected)
            {
                
                button.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                isDessertSelected = FALSE;
            }
            else{
                
                dessert.backgroundColor = [UIColor orangeColor];
                [dessert setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                [button_second_array removeLastObject];
                [button_second_array addObject:yourNumber];
                
                
                isDessertSelected = TRUE;
                
                isVegSelected=FALSE;   isNonVegSelected = FALSE; isVeganSelected = FALSE; isJainSelected=FALSE; isAppetizerSelected=FALSE; isSaladSelected=FALSE; isMainsSelected=FALSE; isIndianSelected=FALSE; isAsianSelected=FALSE; isContinentalSelected= FALSE; isOtherSelected=FALSE;
                isDairyFreeSelected = FALSE; isNutFreeSelected = FALSE; isEgglessSelected= FALSE;
                isGluttenFreeSelected = FALSE;
                
                [appetizer setSelected:NO];
                appetizer.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [appetizer setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                [salad setSelected:NO];
                salad.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [salad setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                [mains setSelected:NO];
                mains.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [mains setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
            
            
            break;
            
        case 8:
            NSLog(@"indian");
            
            
            if(isIndianSelected)
            {
                
                button.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                isIndianSelected = FALSE;
            }
            else{
                
                indian.backgroundColor = [UIColor orangeColor];
                [indian setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                [button_third_array removeLastObject];
                [button_third_array addObject:yourNumber];
                
                
                isIndianSelected = TRUE;
                
                isVegSelected=FALSE;   isNonVegSelected = FALSE; isVeganSelected = FALSE; isJainSelected=FALSE; isAppetizerSelected=FALSE; isSaladSelected=FALSE; isMainsSelected=FALSE; isDessertSelected=FALSE; isAsianSelected=FALSE; isContinentalSelected= FALSE; isOtherSelected=FALSE;
                isDairyFreeSelected = FALSE; isNutFreeSelected = FALSE; isEgglessSelected= FALSE;
                isGluttenFreeSelected = FALSE;
                
                [asian setSelected:NO];
                asian.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [asian setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                [continental setSelected:NO];
                continental.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [continental setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                [other setSelected:NO];
                other.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [other setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
            
            
            break;
            
        case 9:
            NSLog(@"asian");
            
            
            if(isAsianSelected)
            {
                
                button.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                isAsianSelected = FALSE;
            }
            else{
                
                asian.backgroundColor = [UIColor orangeColor];
                [asian setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                [button_third_array removeLastObject];
                [button_third_array addObject:yourNumber];
                
                
                isAsianSelected = TRUE;
                
                isVegSelected=FALSE;   isNonVegSelected = FALSE; isVeganSelected = FALSE; isJainSelected=FALSE; isAppetizerSelected=FALSE; isSaladSelected=FALSE; isMainsSelected=FALSE; isDessertSelected=FALSE; isIndianSelected=FALSE; isContinentalSelected= FALSE; isOtherSelected=FALSE;
                isDairyFreeSelected = FALSE; isNutFreeSelected = FALSE; isEgglessSelected= FALSE;
                isGluttenFreeSelected = FALSE;
                
                [indian setSelected:NO];
                indian.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [indian setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                [continental setSelected:NO];
                continental.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [continental setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                [other setSelected:NO];
                other.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [other setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
            
            break;
            
        case 10:
            NSLog(@"continental");
            
            
            if(isContinentalSelected)
            {
                
                button.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                isContinentalSelected = FALSE;
            }
            else{
                
                continental.backgroundColor = [UIColor orangeColor];
                [continental setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                [button_third_array removeLastObject];
                [button_third_array addObject:yourNumber];
                
                
                isContinentalSelected = TRUE;
                
                isVegSelected=FALSE;   isNonVegSelected = FALSE; isVeganSelected = FALSE; isJainSelected=FALSE; isAppetizerSelected=FALSE; isSaladSelected=FALSE; isMainsSelected=FALSE; isDessertSelected=FALSE; isIndianSelected=FALSE; isAsianSelected= FALSE; isOtherSelected=FALSE;
                isDairyFreeSelected = FALSE; isNutFreeSelected = FALSE; isEgglessSelected= FALSE;
                isGluttenFreeSelected = FALSE;
                
                [indian setSelected:NO];
                indian.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [indian setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                [asian setSelected:NO];
                asian.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [asian setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                [other setSelected:NO];
                other.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [other setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
            
            break;
            
        case 11:
            NSLog(@"other");
            
            
            if(isOtherSelected)
            {
                
                button.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                isOtherSelected = FALSE;
            }
            else{
                
                other.backgroundColor = [UIColor orangeColor];
                [other setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                [button_third_array removeLastObject];
                [button_third_array addObject:yourNumber];
               
                
                isOtherSelected = TRUE;
                
                isVegSelected=FALSE;   isNonVegSelected = FALSE; isVeganSelected = FALSE; isJainSelected=FALSE; isAppetizerSelected=FALSE; isSaladSelected=FALSE; isMainsSelected=FALSE; isDessertSelected=FALSE; isIndianSelected=FALSE; isAsianSelected=FALSE; isContinentalSelected= FALSE;
                isDairyFreeSelected = FALSE; isNutFreeSelected = FALSE; isEgglessSelected= FALSE;
                isGluttenFreeSelected = FALSE;
                
                
                [indian setSelected:NO];
                indian.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [indian setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                [asian setSelected:NO];
                asian.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [asian setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                [continental setSelected:NO];
                continental.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [continental setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                
                
            }
            
            break;
            
        case 12:
            NSLog(@"dairy-free");
            
         if( isDairyFreeSelected )
         {
             [button_fourth_array removeObject:yourNumber];
             dairyFree.backgroundColor = [UIColor groupTableViewBackgroundColor];
             [dairyFree setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
             isDairyFreeSelected = FALSE;
             [final_valueDic removeObjectForKey:@"fourthRow_firstButton"];
         }
         else
         {
             
             [button_fourth_array addObject:yourNumber];
             isDairyFreeSelected = TRUE;
             dairyFree.backgroundColor = [UIColor orangeColor];
             [dairyFree setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
             
         }
             
            break;
            
        case 13:
            NSLog(@"nut-free");
            
            if( isNutFreeSelected )
            {
                [button_fourth_array removeObject:yourNumber];
                nutFree.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [nutFree setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                isNutFreeSelected = FALSE;
                [final_valueDic removeObjectForKey:@"fourthRow_secondButton"];
            }
            else
            {
                [button_fourth_array addObject:yourNumber];
                
                isNutFreeSelected = TRUE;
                nutFree.backgroundColor = [UIColor orangeColor];
                [nutFree setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
            }
             
            break;
            
        case 14:
            NSLog(@"eggless");
            
            if( isEgglessSelected )
            {
                [button_fourth_array removeObject:yourNumber];
                
                eggless.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [eggless setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                isEgglessSelected = FALSE;
                [final_valueDic removeObjectForKey:@"fourthRow_thirdButton"];
            }
            else
            {
                [button_fourth_array addObject:yourNumber];
                isEgglessSelected = TRUE;
                eggless.backgroundColor = [UIColor orangeColor];
                [eggless setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
            }
             
            break;
            
        case 15:
            NSLog(@"gluten-free");
            
            if( isGluttenFreeSelected )
            {
                [button_fourth_array removeObject:yourNumber];
                
                glutenFree.backgroundColor = [UIColor groupTableViewBackgroundColor];
                [glutenFree setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                isGluttenFreeSelected = FALSE;
                [final_valueDic removeObjectForKey:@"fourthRow_fourthButton"];
            }
            else
            {
                [button_fourth_array addObject:yourNumber];
                isGluttenFreeSelected = TRUE;
                glutenFree.backgroundColor = [UIColor orangeColor];
                [glutenFree setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
            }
            
            break;
            
        default:
            break;
    }
    
    NSLog(@"first array %@",button_first_array);
    NSLog(@"second array %@",button_second_array);
    NSLog(@"third array %@",button_third_array);
    NSLog(@"fourth array %@",button_fourth_array);
    
    [self first_row_validation];
    [self second_row_validation];
    [self third_row_validation];
    
    
    
    if(progressView.progress >= 1.0f)
    {
        sell_button.enabled = YES;
        NSArray *first_merge = [button_first_array arrayByAddingObjectsFromArray:button_second_array];
        NSArray *second_merge = [button_third_array arrayByAddingObjectsFromArray:first_merge];
        NSArray *final_merge = [button_fourth_array arrayByAddingObjectsFromArray:second_merge];
        NSLog(@"final array %@",final_merge);
        [self logix:final_merge];
    }
    
}



-(void)logix:(NSArray *)array
{
    for(int i =0;i<[main_array count];i++)
    {
        for(int j=0; j<[array count];j++)
        {
            NSString *str1= [NSString stringWithFormat:@"%@",[main_array objectAtIndex:i]];
            NSString *str2= [NSString stringWithFormat:@"%@",[array objectAtIndex:j]];
            
            if([str1 isEqualToString:str2])
            {
                NSLog(@"found %i",[str2 intValue]);
                [self button_value_ToDic:[str2 intValue]];
                
                
                
            }
            
            
            
        }
    }
    
//    NSLog(@"dic values %@",[final_valueDic allValues]);
    NSLog(@"dic values %@",final_valueDic);
    

}


-(void)button_value_ToDic:(int)number{
    
    switch (number) {
        case 0:
            [final_valueDic setValue:@"veg" forKey:@"firstRow_buttonText"];
            break;
        case 1:
            [final_valueDic setValue:@"nonVeg" forKey:@"firstRow_buttonText"];
            break;
        case 2:
            [final_valueDic setValue:@"vegan" forKey:@"firstRow_buttonText"];
            break;
        case 3:
            [final_valueDic setValue:@"jain" forKey:@"firstRow_buttonText"];
            break;
            
        case 4:
            [final_valueDic setValue:@"appetizer" forKey:@"secondRow_buttonText"];
            break;
        case 5:
            [final_valueDic setValue:@"salad" forKey:@"secondRow_buttonText"];
            break;
        case 6:
            [final_valueDic setValue:@"mains" forKey:@"secondRow_buttonText"];
            break;
        case 7:
            [final_valueDic setValue:@"dessert" forKey:@"secondRow_buttonText"];
            break;
            
            
        case 8:
            [final_valueDic setValue:@"indian" forKey:@"thirdRow_buttonText"];
            break;
        case 9:
            [final_valueDic setValue:@"asian" forKey:@"thirdRow_buttonText"];
            break;
        case 10:
            [final_valueDic setValue:@"continental" forKey:@"thirdRow_buttonText"];
            break;
        case 11:
            [final_valueDic setValue:@"other" forKey:@"thirdRow_buttonText"];
            break;
          
        case 12:
            [final_valueDic setValue:@"dairyFree" forKey:@"fourthRow_firstButton"];
            break;
        case 13:
            [final_valueDic setValue:@"nutFree" forKey:@"fourthRow_secondButton"];
            break;
        case 14:
            [final_valueDic setValue:@"eggless" forKey:@"fourthRow_thirdButton"];
            break;
        case 15:
            [final_valueDic setValue:@"glutenFree" forKey:@"fourthRow_fourthButton"];
            break;
            
        default:
            break;
    }
}

#pragma mark Bottom Button Validations

-(void)first_row_validation{
    
    if([button_first_array count] !=0)
    {
    
    for(int i =0;i<[button_first_array count];i++)
    {
        for(int j=0; j<[first_array count];j++)
        {
            NSString *str1= [NSString stringWithFormat:@"%@",[button_first_array objectAtIndex:i]];
            NSString *str2= [NSString stringWithFormat:@"%@",[first_array objectAtIndex:j]];
            
            if([str1 isEqualToString:str2])
            {
                NSLog(@"found");
                isFirstRowValidated = TRUE;
                
                if(isFirstRow_firstFound == FALSE)
                {
                x = x+0.17f;
                progressView.progress = x;
                }
                
                isFirstRow_firstFound = TRUE;
                
                break;
            }
            

            
        }
    }
    }
    else{
        NSLog(@"Not found");
    }
   
}

-(void)second_row_validation{
    
    
    
    for(int i =0;i<[button_second_array count];i++)
    {
        for(int j=0; j<[second_array count];j++)
        {
            NSString *str1= [NSString stringWithFormat:@"%@",[button_second_array objectAtIndex:i]];
            NSString *str2= [NSString stringWithFormat:@"%@",[second_array objectAtIndex:j]];
            
            if([str1 isEqualToString:str2])
            {
                NSLog(@"found");
                isSecondRowValidated = TRUE;
                
                if(isSecondRow_firstFound == FALSE)
                {
                x = x+0.17f;
                progressView.progress = x;
                }
                
                isSecondRow_firstFound = TRUE;
                
                break;
            }
            
            
        }
    }
    
    
}

-(void)third_row_validation{
    
   
    
    for(int i =0;i<[button_third_array count];i++)
    {
        for(int j=0; j<[third_array count];j++)
        {
            NSString *str1= [NSString stringWithFormat:@"%@",[button_third_array objectAtIndex:i]];
            NSString *str2= [NSString stringWithFormat:@"%@",[third_array objectAtIndex:j]];
            
            if([str1 isEqualToString:str2])
            {
                NSLog(@"found");
                isThirdRowValidated = TRUE;
                
        
                if(isThirdRow_firstFound == FALSE)
                {
                x = x+0.17f;
                progressView.progress = x;
                }
                
                isThirdRow_firstFound = TRUE;
                
                break;
            }
            
            
        }
    }
}


#pragma mark Serve Left+Right Logic

- (IBAction)serve_leftButton:(id)sender
{
    serve_number = serve_centerLabel_text.text;
    serve_num = [serve_number intValue];
    
    if(serve_num>1)
    {
        
        serve_num = serve_num-1;
        
        serve_value = [@(serve_num) stringValue];
        
        if([serve_value intValue]== 1)
        {
            serve_centerLabel_text.text = [serve_value stringByAppendingString:@" person"];
        }
        else
        {
        
        serve_centerLabel_text.text = [serve_value stringByAppendingString:@" persons"];
        }
    }
}

- (IBAction)serve_rightButton:(id)sender
{
    serve_number = serve_centerLabel_text.text;
    serve_num = [serve_number intValue];
    
    if(serve_num<20)
    {
        
        serve_num = serve_num+1;
        
        serve_value = [@(serve_num) stringValue];
        
        serve_centerLabel_text.text = [serve_value stringByAppendingString:@" persons"];
    }
}


#pragma mark Valid Left+Right Logic

- (IBAction)valid_leftButton:(id)sender
{
    valid_number = valid_centerLabel_text.text;
    valid_num = [valid_number intValue];
    
    if(valid_num > 1)
    {
        
        valid_num = valid_num-1;
        
        valid_value = [@(valid_num) stringValue];
        
        if([valid_value intValue]== 1)
        {
            valid_centerLabel_text.text = [valid_value stringByAppendingString:@" hour"];
        }
        else
        {
        valid_centerLabel_text.text = [valid_value stringByAppendingString:@" hours"];
        }
      //  [final_valueDic setValue:[NSNumber numberWithInt:[valid_value intValue]] forKey:@"valid"];
    }
}

- (IBAction)valid_rightButton:(id)sender
{
    
    valid_number = valid_centerLabel_text.text;
    valid_num = [valid_number intValue];
    
    if(valid_num<12)
    {
        
        valid_num = valid_num+1;
        
        valid_value = [@(valid_num) stringValue];
        
        valid_centerLabel_text.text = [valid_value stringByAppendingString:@" hours"];
        
     //   [final_valueDic setValue:[NSNumber numberWithInt:[valid_value intValue]] forKey:@"valid"];
    }
}


#pragma mark Advance Left+Right Logic

- (IBAction)advance_leftButton:(id)sender
{
    advance_num = a;
    
    if(advance_num>15)
    {
        
        advance_num = advance_num-15;
        a = advance_num;
        
        dic= [self time_logic:advance_num];
        
        
        
        NSString *h =  [[dic valueForKey:@"hour"]stringValue];
        NSString *m =   [[dic valueForKey:@"minutes"] stringValue];
        
        
        if ([h isEqual:@"0"]) {
            advance_centerLabel_text.text = [m stringByAppendingString:@" mins"];
        }
        else{
            
            if([m isEqual:@"0"])
            {
                if([h isEqual:@"1"])
                {
                    advance_centerLabel_text.text = [NSString stringWithFormat:@" %@ hour",h];
                }
                else
                {
                    advance_centerLabel_text.text = [NSString stringWithFormat:@" %@ hours",h];
                }
            }
            else
            {
                advance_centerLabel_text.text = [NSString stringWithFormat:@" %@ h %@ mins",h,m];
            }
            
        }
        
    }
}

- (IBAction)advance_rightButon:(id)sender
{
    
    advance_num = a;
    
    if(advance_num<720)
    {
        
        advance_num = advance_num+15;
        a = advance_num;
        
       dic= [self time_logic:advance_num];
        
        
        NSString *h =  [[dic valueForKey:@"hour"]stringValue];
        NSString *m =   [[dic valueForKey:@"minutes"] stringValue];
        
        if ([h isEqual:@"0"]) {
            advance_centerLabel_text.text = [m stringByAppendingString:@" mins"];
        }
        else{
            
            if([m isEqual:@"0"])
            {
                if([h isEqual:@"1"])
                {
                advance_centerLabel_text.text = [NSString stringWithFormat:@" %@ hour",h];
                }
                else
                {
                    advance_centerLabel_text.text = [NSString stringWithFormat:@" %@ hours",h];
                }
            }
            else
            {
            advance_centerLabel_text.text = [NSString stringWithFormat:@" %@ h %@ mins",h,m];
            }
            
        }
        
    }
}


-(NSMutableDictionary*)time_logic:(NSInteger)number{

    int minutes = number%60;
    long hour = (number-minutes)/60;
    
    [dic setValue:[NSNumber numberWithLong:hour] forKey:@"hour"];
    [dic setValue:[NSNumber numberWithInt:minutes] forKey:@"minutes"];
    
    NSLog(@"%li hour %i minutes",hour,minutes);
    
    return dic;
    
}

#pragma mark Price Left+Right Logic

- (IBAction)price_leftButton:(id)sender
{
    [self price_leftButtonLogic];
}

- (IBAction)price_right_button:(id)sender
{
        [self price_rightButtonLogic];
}


-(void)price_leftButtonLogic{
    price_number = price_centerLabel_text.text;
    price_num = [price_number intValue];
    
    if(price_num>25)
    {
        
        price_num = price_num-25;
        
        price_value = [@(price_num) stringValue];
        
        price_centerLabel_text.text = price_value;
    }
}

-(void)price_rightButtonLogic
{
    price_number = price_centerLabel_text.text;
    price_num = [price_number intValue];
    
    if(price_num<3000)
    {
        
        price_num = price_num+25;
        
        price_value = [@(price_num) stringValue];
        
        price_centerLabel_text.text = price_value;
    }
}

#pragma mark Long Press on Price Left+Right Logic

-(void)rightLongPress:(UILongPressGestureRecognizer *)gesture {
    if ( gesture.state == UIGestureRecognizerStateBegan ) {
        NSLog(@"Long Press");
        
        timer = [NSTimer scheduledTimerWithTimeInterval:.1 target:self selector:@selector(price_rightButtonLogic) userInfo:nil repeats:YES];
        
    }
    
    if(gesture.state == UIGestureRecognizerStateEnded)
    {
        [timer invalidate];
    }
}

-(void)leftLongPress:(UILongPressGestureRecognizer *)gesture
{
    if ( gesture.state == UIGestureRecognizerStateBegan ) {
        NSLog(@"Long Press");
        
        timer = [NSTimer scheduledTimerWithTimeInterval:.1 target:self selector:@selector(price_leftButtonLogic) userInfo:nil repeats:YES];
        
    }
    
    if(gesture.state == UIGestureRecognizerStateEnded)
    {
        [timer invalidate];
    }
}

#pragma mark Sell Button Click

-(void)sell
{
    [final_valueDic setValue:price_centerLabel_text.text forKey:@"price"];
    [final_valueDic setValue:valid_centerLabel_text.text forKey:@"valid"];
    [final_valueDic setValue:serve_centerLabel_text.text forKey:@"serving"];
    [final_valueDic setValue:advance_centerLabel_text.text forKey:@"advance"];
    
    NSLog(@"NOW THE FINAL DIC VALUES IS %@", final_valueDic);
    
}

#pragma mark Memory Warning

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
