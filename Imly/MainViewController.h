//
//  MainViewController.h
//  Imly
//
//  Created by Ankur Khanna on 21/03/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DiscoverViewController.h"
#import "FavoriteViewController.h"
#import "AddViewController.h"
#import "NotificationViewController.h"
#import "ProfileViewController.h"
#import "PostDishViewController.h"

@interface MainViewController : UIViewController <UITabBarDelegate>


//@property (nonatomic, retain) IBOutlet UITabBar *mainTabBar;

@property (nonatomic, strong) UITabBarController *tab;


@property (nonatomic, strong) DiscoverViewController *discoverViewController; //0

@property (nonatomic, strong) FavoriteViewController *favoriteViewController; //1

@property (nonatomic, strong) AddViewController *addViewController; //2

@property (nonatomic, strong) NotificationViewController *notificationViewController; //3

@property (nonatomic, strong) ProfileViewController *profileViewController; //4

@property (nonatomic, strong) PostDishViewController *postViewController;




@end
