//
//  IntroViewController.h
//  Imly
//
//  Created by Ankur Khanna on 18/03/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroViewController : UIViewController <UIScrollViewDelegate>





@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;



- (IBAction)discover_Imly:(id)sender;

- (IBAction)signUp:(id)sender;

- (IBAction)signIn:(id)sender;

@end
