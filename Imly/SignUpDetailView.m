//
//  SignUpDetailView.m
//  Imly
//
//  Created by Ankur Khanna on 20/03/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import "SignUpDetailView.h"
#import "myMacros.h"

#import "IntroViewController.h"

#import "MainViewController.h"


@interface SignUpDetailView (){
    BOOL isPasswordValidated;
    
    UIButton *signUp_button;
    
      NSMutableData *_responseData;
    
}

@end

@implementation SignUpDetailView

@synthesize navBar,name_text,password_text,signUp_Dict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
//        UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 60.0f, 30.0f)];
//        UIImage *backImage = [[UIImage imageNamed:@"skip.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 12.0f, 0, 12.0f)];
//        [backButton setBackgroundImage:backImage  forState:UIControlStateNormal];
//        [backButton setTitle:@"Close" forState:UIControlStateNormal];
//        [backButton addTarget:self action:@selector(closeTo) forControlEvents:UIControlEventTouchUpInside];
//        UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
//        self.navigationItem.rightBarButtonItem = backButtonItem;
        
        
//        self.navigationItem.title = @"Sign Up";
        
        // Custom initialization
//        navBar = [[UINavigationBar alloc]initWithFrame:CGRectMake(0,0 , self.view.frame.size.width, 66)];
//        [navBar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
//        [self.view addSubview:navBar];
        
        //        UIButton* fakeButton = (UIButton *) [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"yourimage.png"]];
        
        
//        UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(back)];
//        
//         UIBarButtonItem *closetoRoot = [[UIBarButtonItem alloc]initWithTitle:@"X" style:UIBarButtonItemStylePlain target:self action:@selector(closeTo)];
//        
//        
//        UINavigationItem *navItem = [[UINavigationItem alloc]initWithTitle:@"Sign Up"];
//        [navItem setLeftBarButtonItem:backItem animated:YES];
//        [navItem setRightBarButtonItem:closetoRoot animated:YES];
//        [navBar setItems:[NSArray arrayWithObject:navItem] animated:YES];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isPasswordValidated = NO;
    
//    name_text = [[UITextField alloc]init];
//    name_text.placeholder = @"Enter your Name";
//    [name_text setBorderStyle:UITextBorderStyleRoundedRect];
//    name_text.backgroundColor = [UIColor lightGrayColor];
//    name_text.delegate = self;
//    name_text.tag = 1;
//    [self.view addSubview:name_text];
//    
//    password_text = [[UITextField alloc]init];
//    [password_text setSecureTextEntry:YES];
//    password_text.placeholder = @"Enter Password";
//    [password_text setBorderStyle:UITextBorderStyleRoundedRect];
//    password_text.backgroundColor = [UIColor lightGrayColor];
//    password_text.delegate = self;
//    password_text.tag = 2;
//    [self.view addSubview:password_text];
//    
//    
//    
//    signUp_button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [signUp_button setBackgroundColor:[UIColor orangeColor]];
//    [signUp_button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14]];
//    [signUp_button setTitle:@"Sign Up" forState:UIControlStateNormal];
//    [signUp_button addTarget:self
//                        action:@selector(signUp_button_clicked)
//              forControlEvents:UIControlEventTouchUpInside];
//    //  facebook_button.translatesAutoresizingMaskIntoConstraints = NO;
//    [self.view addSubview:signUp_button];
    
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark Text Field Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
   
    textField.placeholder = nil;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if(textField.tag ==1)
    {
    textField.placeholder = @"Enter your Name";
    }
    
    
    if(textField.tag ==2)
    {
      
         textField.placeholder = @"Enter Password";  
       
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
   
    [textField resignFirstResponder];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *t = [[event allTouches] anyObject];
    
    if ([name_text isFirstResponder] && [t view] != name_text) {
        [name_text resignFirstResponder];
    }
    if ([password_text isFirstResponder] && [t view] != password_text) {
        [password_text resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}





- (IBAction)signUp_button_clicked:(id)sender {
    
    [name_text resignFirstResponder];
    [password_text resignFirstResponder];
    
    NSLog(@"Sign Up button clicked");
    
    if([name_text.text isEqual:@""] || [password_text.text isEqual:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Fields"
                                                        message:@"Please Fill in the fields"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    if(![password_text.text isEqual:@""])
    {
        
        if([password_text.text length]>=6)
        {
            isPasswordValidated =YES;
            
          //  [signUp_Dict setValue:name_text.text forKey:@"signUp_name"];
            [signUp_Dict setValue:password_text.text forKey:@"password"];
            
            NSLog(@"total values in the dict are %@",signUp_Dict);
            
        
            NSString *jsonString =        [signUp_Dict jsonStringWithPrettyPrint:NO];
        
            
            [self sendServer:jsonString];
            
            
           // [self JsonLogic:signUp_Dict];
            
            
            
           
            
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password"
                                                            message:@"Please Enter at least 6 characters"
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
    
}

//-(void)JsonLogic:(NSMutableDictionary *)dict{
//    NSError *error;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
//                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
//                                                         error:&error];
//    
//    if (! jsonData) {
//        NSLog(@"Got an error: %@", error);
//    } else {
//        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//        NSLog(@"in json format %@",jsonString);
//        
//        NSString * newReplacedString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
//        NSLog(@"string is %@",newReplacedString);
//        
//        [self sendServer:newReplacedString];
//    }
//}

-(void)sendServer:(NSString*)jsonString
{
    
    
    NSString *post = [NSString stringWithFormat:@"http://192.168.10.64:8000/accounts/"];
    
    NSString *paramter = jsonString;
    
    NSData *postData = [paramter dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
  
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:post]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    
 
    
    NSURLSessionConfiguration *config =[NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session =
    [NSURLSession sessionWithConfiguration:config
                                  delegate:self
                             delegateQueue:nil];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *connectionError) {
    
                // handle response
                NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
                int code = [httpResponse statusCode];
                NSLog(@"code %d",code);
        
        if (code ==400)
        {
            [self performSelectorOnMainThread:@selector(showAlert) withObject:nil waitUntilDone:NO];
            
           
        }
        
                if ([data length] >0 && connectionError == nil && code == 201)
                {
                    // response comes in here
                
                   
                    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                   [self saveToUserDefaults:[json valueForKey:@"token"]];
                    
                    MainViewController *mvc = [[MainViewController alloc]initWithNibName:@"MainViewController" bundle:nil];
                    [self presentViewController:mvc animated:YES completion:nil];
                }
                else if ([data length] == 0 && connectionError == nil)
                {
                    NSLog(@"There was nothing to download");
                }
                else if (connectionError != nil)
                {
                    NSLog(@"Error = %@", connectionError);
                }
        
            
            }] resume];

    
}


-(void)saveToUserDefaults:(NSString*)token
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    if (standardUserDefaults) {
        [standardUserDefaults setObject:token forKey:@"Token_Value"];
        [standardUserDefaults synchronize];
    }
    
}

-(void)showAlert
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Already Registered" message:@"Use another Email id" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}



- (IBAction)close:(id)sender
{
    self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
