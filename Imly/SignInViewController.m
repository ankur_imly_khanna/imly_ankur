//
//  SignInViewController.m
//  Imly
//
//  Created by Ankur Khanna on 21/03/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import "SignInViewController.h"
#import "MainViewController.h"


@interface SignInViewController ()
{
    BOOL isPasswordValidated;
    BOOL isEmailValidated;
    
    UIButton *signIn_button;
    UILabel *or_label;
    UIButton *facebook_button;
}

@end

@implementation SignInViewController

@synthesize email_text_signIn,password_text_signIn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isPasswordValidated = NO;
    
//    email_text_signIn = [[UITextField alloc]init];
//    email_text_signIn.placeholder = @"Enter Email Address";
//    [email_text_signIn setBorderStyle:UITextBorderStyleRoundedRect];
//    email_text_signIn.backgroundColor = [UIColor lightGrayColor];
//    email_text_signIn.delegate = self;
//    email_text_signIn.tag = 1;
//    [self.view addSubview:email_text_signIn];
//    
//    password_text_signIn = [[UITextField alloc]init];
//    [password_text_signIn setSecureTextEntry:YES];
//    password_text_signIn.placeholder = @"Enter Password";
//    [password_text_signIn setBorderStyle:UITextBorderStyleRoundedRect];
//    password_text_signIn.backgroundColor = [UIColor lightGrayColor];
//    password_text_signIn.delegate = self;
//    password_text_signIn.tag = 2;
//    [self.view addSubview:password_text_signIn];
//    
//    
//    
//    signIn_button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [signIn_button setBackgroundColor:[UIColor orangeColor]];
//    [signIn_button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14]];
//    [signIn_button setTitle:@"Sign In" forState:UIControlStateNormal];
//    [signIn_button addTarget:self
//                      action:@selector(signIn_button_clicked)
//            forControlEvents:UIControlEventTouchUpInside];
//    
//    [self.view addSubview:signIn_button];
//    
//    
//    
//    
//    // Left Line of OR
//    
//    if(IsIphone5)
//    {
//        UIBezierPath *path = [UIBezierPath bezierPath];
//        [path moveToPoint:CGPointMake(40, 270)];
//        [path addLineToPoint:CGPointMake(140, 270)];
//        
//        CAShapeLayer *shapeLayer = [CAShapeLayer layer];
//        shapeLayer.path = [path CGPath];
//        shapeLayer.strokeColor = [[UIColor darkGrayColor] CGColor];
//        shapeLayer.lineWidth = 1.0;
//        shapeLayer.fillColor = [[UIColor clearColor] CGColor];
//        [self.view.layer addSublayer:shapeLayer];
//        
//        
//        // Right Line of OR
//        
//        UIBezierPath *path2 = [UIBezierPath bezierPath];
//        [path2 moveToPoint:CGPointMake(280, 270.0)]; //(lenght choti, )
//        [path2 addLineToPoint:CGPointMake(175.0, 270.0)]; //
//        
//        CAShapeLayer *shapeLayer2 = [CAShapeLayer layer];
//        shapeLayer2.path = [path2 CGPath];
//        shapeLayer2.strokeColor = [[UIColor darkGrayColor] CGColor];
//        shapeLayer2.lineWidth = 1.0;
//        shapeLayer2.fillColor = [[UIColor clearColor] CGColor];
//        [self.view.layer addSublayer:shapeLayer2];
//        
//    }
//    
//    else
//    {
//        
//        
//        UIBezierPath *path = [UIBezierPath bezierPath];
//        [path moveToPoint:CGPointMake(40, 215)];
//        [path addLineToPoint:CGPointMake(140, 215)];
//        
//        CAShapeLayer *shapeLayer = [CAShapeLayer layer];
//        shapeLayer.path = [path CGPath];
//        shapeLayer.strokeColor = [[UIColor darkGrayColor] CGColor];
//        shapeLayer.lineWidth = 1.0;
//        shapeLayer.fillColor = [[UIColor clearColor] CGColor];
//        [self.view.layer addSublayer:shapeLayer];
//        
//        
//        // Right Line of OR
//        
//        UIBezierPath *path2 = [UIBezierPath bezierPath];
//        [path2 moveToPoint:CGPointMake(280, 215.0)]; //(lenght choti, )
//        [path2 addLineToPoint:CGPointMake(175.0, 215.0)]; //
//        
//        CAShapeLayer *shapeLayer2 = [CAShapeLayer layer];
//        shapeLayer2.path = [path2 CGPath];
//        shapeLayer2.strokeColor = [[UIColor darkGrayColor] CGColor];
//        shapeLayer2.lineWidth = 1.0;
//        shapeLayer2.fillColor = [[UIColor clearColor] CGColor];
//        [self.view.layer addSublayer:shapeLayer2];
//    }
//    
//    // Label of OR
//    
//    or_label = [[UILabel alloc]init];
//    or_label.text = @"OR";
//    [self.view addSubview:or_label];
//    
//    
//    
//    facebook_button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [facebook_button setBackgroundColor:[UIColor orangeColor]];
//    [facebook_button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14]];
//    [facebook_button setTitle:@"Facebook" forState:UIControlStateNormal];
//    [facebook_button addTarget:self
//                        action:@selector(facebook_signIn_button_clicked)
//              forControlEvents:UIControlEventTouchUpInside];
//    //  facebook_button.translatesAutoresizingMaskIntoConstraints = NO;
//    [self.view addSubview:facebook_button];
    
    
    
    
    // Do any additional setup after loading the view from its nib.
}


//#pragma mark Setting Frame
//
//- (void)viewWillLayoutSubviews {
//    if ([UIScreen mainScreen].bounds.size.height == 568) {
//        email_text_signIn.frame= CGRectMake(60, 120, 210, 30);
//        password_text_signIn.frame = CGRectMake(60, 160, 210, 30);
//        signIn_button.frame = CGRectMake(60, 220, 210, 30);
//        or_label.frame = CGRectMake(145, 250, 40, 40);
//        facebook_button.frame = CGRectMake(60, 300, 210, 30);
//        
//    } else {
//        email_text_signIn.frame = CGRectMake(60, 90, 210, 30);
//        password_text_signIn.frame = CGRectMake(60, 130, 210, 30);
//        signIn_button.frame = CGRectMake(60, 170, 210, 30);
//        or_label.frame = CGRectMake(145, 195, 40, 40);
//        facebook_button.frame = CGRectMake(60, 230, 210, 30);
//        
//        
//    }
//}


#pragma mark Text Field Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    textField.placeholder = nil;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if(textField.tag ==1)
    {
        textField.placeholder = @"Enter Email Address";
    }
    
    
    if(textField.tag ==2)
    {
        
        textField.placeholder = @"Enter Password";
        
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *t = [[event allTouches] anyObject];
    
    if ([email_text_signIn isFirstResponder] && [t view] != email_text_signIn) {
        [email_text_signIn resignFirstResponder];
    }
    if ([password_text_signIn isFirstResponder] && [t view] != password_text_signIn) {
        [password_text_signIn resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}


#pragma mark Validate Email

- (BOOL) validateEmail: (NSString *) emailStr {
	
    
	NSString *emailvalidate = @"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}";
    
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailvalidate] ;
	
    return [emailTest evaluateWithObject:emailStr];
}








- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)signIn_button_clicked:(id)sender {
    
    NSLog(@"Sign In Clicked");
    
    if([email_text_signIn.text isEqual:@""] || [password_text_signIn.text isEqual:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Fields"
                                                        message:@"Please Fill in the fields"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    if(![password_text_signIn.text isEqual:@""])
    {
        
        if([password_text_signIn.text length]>=6)
        {
            isPasswordValidated =YES;
            if([self validateEmail:email_text_signIn.text])
            {
                isEmailValidated = YES;
                
                if(IsIphone5)
                {
                    MainViewController *mvc = [[MainViewController alloc]initWithNibName:@"MainViewIphone5" bundle:nil];
                    [self presentViewController:mvc animated:YES completion:nil];
                }
                else{
                    
                
                MainViewController *mvc = [[MainViewController alloc]initWithNibName:@"MainViewController" bundle:nil];
                [self presentViewController:mvc animated:YES completion:nil];
                }
                
            }
            else
            {
                UIAlertView *not_validEmail = [[UIAlertView alloc]initWithTitle:@"Invalid Email id" message:@"Enter valid Email Address" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [not_validEmail show];
            }
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password"
                                                            message:@"Please Enter at least 6 characters"
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }

}

- (IBAction)facebook_signIn_button_clicked:(id)sender {
    
    NSLog(@"Facebook Sign In Clicked");
}

- (IBAction)close:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


@end
