//
//  PostDishViewController.m
//  Imly
//
//  Created by Ankur Khanna on 26/03/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import "PostDishViewController.h"

#import "DishDetailsController.h"

#import "UIImage+ImageResize.h"


@interface PostDishViewController ()
{
    UIBarButtonItem *next_button;
    
    NSString *number;
    NSInteger num;
    NSString *value;
    
    NSMutableDictionary *final_valueDic;
}
@end

@implementation PostDishViewController

@synthesize dish_text,progressView,center_label,imageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self.tabBarController.tabBar.hidden = NO;
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
 //   self.navigationController.title = @"Post a Dish";
    
   NSString *str = [[NSUserDefaults standardUserDefaults]stringForKey:@"Token_Value"];
    NSLog(@"token is %@",str);
    
    final_valueDic = [[NSMutableDictionary alloc]init];
    
    self.navigationItem.title = @"Post a Dish";
    
    [imageView setUserInteractionEnabled:YES];
    
    next_button = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(next)];
    next_button.tintColor = [UIColor redColor];
    self.navigationItem.rightBarButtonItem = next_button;
    next_button.enabled = NO;
    
    center_label.text = @"1";
    
    [final_valueDic setValue:center_label.text forKey:@"dishes"];
    [final_valueDic setValue:@"" forKey:@"dish_Image"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(eventListenerDidReceiveNotification:)
                                                 name:@"MyNotification"
                                               object:nil];
    
    [center_label setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"textbox"]]];
    // Do any additional setup after loading the view from its nib.
}



#pragma mark Text Field Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    textField.placeholder = nil;

}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    
       if( ![textField.text isEqual:@""])
       {
           progressView.progress = 0.5f;
           next_button.enabled = YES;
           
       }
       else{
           textField.placeholder = @"Name of your Dish";
           progressView.progress = 0.1f;
           next_button.enabled = NO;
       }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *t = [[event allTouches] anyObject];
    
    if ([dish_text isFirstResponder] && [t view] != dish_text) {
        [dish_text resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // Logic when a first charcter comes in the ext foeld the button gets enabled with increase in progress view.
    
    NSUInteger length = dish_text.text.length - range.length + string.length;
    if (length > 0) {
        next_button.enabled = YES;
        progressView.progress = 0.5f;
        
    } else {
        next_button.enabled = NO;
        progressView.progress = 0.1f;
    }
    return YES;
}

#pragma mark Next Button Click

-(void)next{
    NSLog(@"clicked next");

    if (progressView.progress == .5f || ![dish_text.text isEqual:@""])
    {
        
        next_button.enabled = YES;
        [final_valueDic setValue:dish_text.text forKey:@"dishName"];
        [final_valueDic setValue:center_label.text forKey:@"dishes"];

        
        DishDetailsController *ddc = [[DishDetailsController alloc]initWithNibName:@"DishDetailsController" bundle:nil];
        
        NSLog(@"Dictionary final values %@",final_valueDic);
        
        ddc.final_valueDic = final_valueDic;
        [self.navigationController pushViewController:ddc animated:YES];
    }
    else
    {
       
        next_button.enabled = NO;
    }

}

#pragma mark Stepper Logic



- (IBAction)left_button:(id)sender {
    
    number = center_label.text;
    num = [number intValue];
    
    if(num>1)
    {
        
        num = num-1;
        
        value = [@(num) stringValue];
        
        center_label.text = value;
    }
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:center_label.text
                                                         forKey:@"dishes"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MyNotification"
                                                        object:nil
                                                      userInfo:userInfo];
}

- (IBAction)right_button:(id)sender {
    
    number = center_label.text;
    num = [number intValue];
    
    if(num<20)
    {
        
        num = num+1;
        
        value = [@(num) stringValue];
        
        center_label.text = value;
    }
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:center_label.text
                                                         forKey:@"dishes"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MyNotification"
                                                        object:nil
                                                      userInfo:userInfo];
}


- (void)eventListenerDidReceiveNotification:(NSNotification *)notif
{
    if ([[notif name] isEqualToString:@"MyNotification"])
    {
        NSLog(@"Successfully received the notification!");
        
        NSDictionary *userInfo = notif.userInfo;
        center_label.text = [userInfo objectForKey:@"dishes"];
        
        // Your response to the notification should be placed here
        NSLog(@"Text Property -> %@", center_label.text);
        
        [final_valueDic setValue:center_label.text forKey:@"dishes"];
        
    }
}

#pragma mark Image Logic

- (IBAction)selectPhoto:(id)sender
{
    [dish_text resignFirstResponder];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Photo Library", nil];
    
    [actionSheet showInView:self.view];
    
    
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    switch (buttonIndex) {
        case 0:
            NSLog(@"Take photo");
            
            [self selected_takePhoto];
            break;
        case 1:
            NSLog(@"photo library");
            
            [self selected_photo_library];
            break;
            
        default:
            break;
    }
    
}

-(void)selected_takePhoto
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:
                                  (NSString *) kUTTypeImage,
                                  nil];
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
        newMedia = YES;
    }
}

-(void)selected_photo_library
{
    
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        UIImagePickerController *pickImage = [[UIImagePickerController alloc]init];
        pickImage.delegate = self;
        pickImage.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        pickImage.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage,nil];
        pickImage.allowsEditing = NO;
        [self presentViewController:pickImage animated:YES completion:nil];
        
        newMedia = NO;
        
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if( [mediaType isEqualToString:(NSString *)kUTTypeImage ])
    {
        
        UIImage *image =[info objectForKey:UIImagePickerControllerOriginalImage];
        UIImage *iconImage = [UIImage imageWithImage:image scaledToSize:CGSizeMake(63, 63)];
        
       
        
        
    
        if (newMedia)
        {
            UIImageWriteToSavedPhotosAlbum(image,
                                           self,
                                           @selector(image:finishedSavingWithError:contextInfo:),
                                           nil);
        }
        
        [self dismissViewControllerAnimated:YES completion:^{
            imageView.image = iconImage;
              [final_valueDic setValue:image forKey:@"originalImage"];
           
            
        }];
        
        
        
        
    }
}


-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"\
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    
    self.tabBarController.tabBar.hidden = NO;
    
}


-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



@end
