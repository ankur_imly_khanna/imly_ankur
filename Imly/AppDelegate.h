//
//  AppDelegate.h
//  Imly
//
//  Created by Ankur Khanna on 18/03/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IntroViewController.h"



@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) IntroViewController *ivc;

@property (strong, nonatomic) UINavigationController *navController;


@end
