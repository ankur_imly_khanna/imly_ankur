//
//  NSMutableDictionary+JSONString.m
//  Imly
//
//  Created by Ankur Khanna on 03/04/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import "NSMutableDictionary+JSONString.h"

@implementation NSMutableDictionary (JSONString)

-(NSString*) jsonStringWithPrettyPrint:(BOOL) prettyPrint
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:(NSJSONWritingOptions)    (prettyPrint ? NSJSONWritingPrettyPrinted : 0)
                                                         error:&error];
    
    if (! jsonData)
    {
        NSLog(@" jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"{}";
    }
    else
    {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

@end
