//
//  DishDetailsController.h
//  Imly
//
//  Created by Ankur Khanna on 26/03/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DishDetailsController : UIViewController
{
    
    BOOL isFirstRowValidated;               // Validation Logic Bool variable
    BOOL isSecondRowValidated;
    BOOL isThirdRowValidated;
    
    
    BOOL isVegSelected;                     // Button Selected or Not Logic Bool's
    BOOL isNonVegSelected;
    BOOL isVeganSelected;
    BOOL isJainSelected;
    
    BOOL isAppetizerSelected;
    BOOL isSaladSelected;
    BOOL isMainsSelected;
    BOOL isDessertSelected;
    
    BOOL isIndianSelected;
    BOOL isAsianSelected;
    BOOL isContinentalSelected;
    BOOL isOtherSelected;
    
    BOOL isDairyFreeSelected;
    BOOL isNutFreeSelected;
    BOOL isEgglessSelected;
    BOOL isGluttenFreeSelected;
    
    
    NSString *serve_number;                 // Logic variable for serve button
    NSInteger serve_num;
    NSString *serve_value;
    
    NSString *valid_number;                 // Logic variable for valid(hours) button
    NSInteger valid_num;
    NSString *valid_value;
    
    NSString *advance_number;               // Logic variable for advance notice period button
    NSInteger advance_num;
    NSString *advance_value;
    
    NSString *price_number;                 // Logic variable for price button
    NSInteger price_num;
    NSString *price_value;
    
}

@property (strong, nonatomic) NSMutableDictionary *final_valueDic;      // final dictionary that would be passed somewhere

@property (strong, nonatomic) IBOutlet UIProgressView *progressView;


@property (strong, nonatomic) IBOutlet UIButton *veg;
@property (strong, nonatomic) IBOutlet UIButton *nonVeg;
@property (strong, nonatomic) IBOutlet UIButton *vegan;
@property (strong, nonatomic) IBOutlet UIButton *jain;


@property (strong, nonatomic) IBOutlet UIButton *appetizer;
@property (strong, nonatomic) IBOutlet UIButton *salad;
@property (strong, nonatomic) IBOutlet UIButton *mains;
@property (strong, nonatomic) IBOutlet UIButton *dessert;


@property (strong, nonatomic) IBOutlet UIButton *indian;
@property (strong, nonatomic) IBOutlet UIButton *asian;
@property (strong, nonatomic) IBOutlet UIButton *continental;
@property (strong, nonatomic) IBOutlet UIButton *other;



@property (strong, nonatomic) IBOutlet UIButton *dairyFree;
@property (strong, nonatomic) IBOutlet UIButton *nutFree;
@property (strong, nonatomic) IBOutlet UIButton *eggless;
@property (strong, nonatomic) IBOutlet UIButton *glutenFree;




- (IBAction)serve_leftButton:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *serve_centerLabel_text;
- (IBAction)serve_rightButton:(id)sender;


- (IBAction)valid_leftButton:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *valid_centerLabel_text;
- (IBAction)valid_rightButton:(id)sender;


- (IBAction)advance_leftButton:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *advance_centerLabel_text;
- (IBAction)advance_rightButon:(id)sender;


- (IBAction)price_leftButton:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *price_centerLabel_text;
- (IBAction)price_right_button:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *price_left;
@property (strong, nonatomic) IBOutlet UIButton *price_right;

@property (strong, nonatomic) NSTimer *timer;



- (IBAction)button_clicked:(id)sender;

@end
