//
//  SignInViewController.h
//  Imly
//
//  Created by Ankur Khanna on 21/03/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignInViewController : UIViewController<UITextFieldDelegate>


@property (strong, nonatomic) IBOutlet UITextField *email_text_signIn;
@property (strong, nonatomic) IBOutlet UITextField *password_text_signIn;

- (IBAction)close:(id)sender;

- (IBAction)signIn_button_clicked:(id)sender;

- (IBAction)facebook_signIn_button_clicked:(id)sender;
@end
