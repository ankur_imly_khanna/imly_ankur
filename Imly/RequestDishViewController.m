//
//  RequestViewController.m
//  Imly
//
//  Created by Ankur Khanna on 24/03/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import "RequestDishViewController.h"

@interface RequestDishViewController ()

@end

@implementation RequestDishViewController

@synthesize request_dish_text;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
  
    // Do any additional setup after loading the view from its nib.
}


#pragma mark Text Field Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    textField.placeholder = nil;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
  
    textField.placeholder = @"What dish are you looking for ?";
    

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    [textField resignFirstResponder];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *t = [[event allTouches] anyObject];
    
    if ([request_dish_text isFirstResponder] && [t view] != request_dish_text) {
        [request_dish_text resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back:(id)sender {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

- (IBAction)next_button_clicked:(id)sender
{
    NSLog(@"next buton clicked");
}
@end
