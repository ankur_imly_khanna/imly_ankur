//
//  AddViewController.h
//  Imly
//
//  Created by Ankur Khanna on 21/03/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddViewController : UIViewController

- (IBAction)request_dish_button_clicked:(id)sender;

- (IBAction)post_dish_button_clicked:(id)sender;

@end
