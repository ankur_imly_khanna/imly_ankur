//
//  IntroViewController.m
//  Imly
//
//  Created by Ankur Khanna on 18/03/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import "IntroViewController.h"

#import "SignUpViewController.h"
#import "SignInViewController.h"

#import "MainViewController.h"

@interface IntroViewController ()

{
    UIImageView *img;
    
}

@end

@implementation IntroViewController

//@synthesize scrollView,pageControl,imageArray;

@synthesize imageView,pageControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    imageArray = [NSArray arrayWithObjects:@"intro_screen2.jpg", @"intro_screen3.jpg", @"intro_screen4.jpg", @"intro_screen5.jpg",nil];
//    
//    
//    // Added an imageview on main view
//    img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    [self.view addSubview:img];
//    img.image = [UIImage imageNamed:@"intro_screen2.jpg"];
//    
//    // Added Pagecontrol over the ImageView
//    pageControl = [[UIPageControl alloc]initWithFrame:CGRectMake(141, self.view.frame.size.height-40, 39, 37)];
//    [pageControl setNumberOfPages:4];
//    pageControl.currentPage=0;
//    [img addSubview:pageControl];
//    
//    
//    
//    // Added a scrollView over the MainView ( Reason: PageCcontrol and ScrollView must be over the same view)
//    
//    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    scrollView.pagingEnabled = YES;
//    scrollView.scrollEnabled= YES;
//    scrollView.backgroundColor = [UIColor clearColor];
//    
//    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * [imageArray count], scrollView.frame.size.height);
//    scrollView.delegate = self;
//    [self.view addSubview:scrollView];
//    
//    
//    // Now Adding the Buttons over the main view for Sign Up, Sign In and Discover.
//    
//    
//    UIButton *signup_button = [UIButton buttonWithType:UIButtonTypeCustom];
//    signup_button.frame = CGRectMake(49, self.view.frame.size.height-60, 65, 30);
//    [signup_button setTitle:@"Sign Up" forState:UIControlStateNormal];
//    [signup_button setBackgroundColor:[UIColor redColor]];
//    [signup_button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14]];
//    [signup_button addTarget:self
//                      action:@selector(signup_clicked)
//       forControlEvents:UIControlEventTouchUpInside];
//    
//    signup_button.userInteractionEnabled = YES;
//    [self.view addSubview:signup_button];
//    
//    
//    
//    UIButton *discover_button = [UIButton buttonWithType:UIButtonTypeCustom];
//    discover_button.frame = CGRectMake(120, self.view.frame.size.height-110, 93, 30);
//    [discover_button setBackgroundColor:[UIColor redColor]];
//    [discover_button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14]];
//   
//    [discover_button setTitle:@"Discover Imly" forState:UIControlStateNormal];
//    [discover_button addTarget:self
//                        action:@selector(discover_clicked)
//            forControlEvents:UIControlEventTouchUpInside];
//    
//    [self.view addSubview:discover_button];
//    
//    
//    
//    
//    UIButton *signin_button = [UIButton buttonWithType:UIButtonTypeCustom];
//    signin_button.frame = CGRectMake(211, self.view.frame.size.height-60, 65, 30);
//    [signin_button setBackgroundColor:[UIColor redColor]];
//    [signin_button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14]];
//    [signin_button setTitle:@"Sign In" forState:UIControlStateNormal];
//    [signin_button addTarget:self
//                      action:@selector(signin_clicked)
//              forControlEvents:UIControlEventTouchUpInside];
//     signin_button.userInteractionEnabled = YES;
//
//    [self.view addSubview:signin_button];
    
    
    
    [imageView setUserInteractionEnabled:YES];
    [pageControl setCurrentPage:0];
    
    [self swipe];
    
    
    // Do any additional setup after loading the view from its nib.
}

//- (void)scrollViewDidScroll:(UIScrollView *)sender
//{
//    // Update the page when more than 50% of the previous/next page is visible
//    CGFloat pageWidth = self.scrollView.frame.size.width;
//    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
//    self.pageControl.currentPage = page;
//    
//    
//    if (page ==0) {
//        img.image = [UIImage imageNamed:@"intro_screen2.jpg"];
//    }
//    else if (page ==1)
//    {
//        img.image = [UIImage imageNamed:@"intro_screen3.jpg"];
//    }
//    else if (page ==2)
//    {
//        img.image = [UIImage imageNamed:@"intro_screen4.jpg"];
//    }
//    else if (page ==3)
//    {
//        img.image = [UIImage imageNamed:@"intro_screen5.jpg"];
//    }
//}
//
//-(void)signup_clicked{
//    NSLog(@"sign up");
//    
//    
//    
//    if (IsIphone5) {
//        SignUpViewController *svc = [[SignUpViewController alloc]initWithNibName:@"SignUpViewIphone5" bundle:nil];
//        svc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//        [self.navigationController setNavigationBarHidden:NO animated:YES];
//        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//        [self.navigationController pushViewController:svc animated:YES];
//        
//       // [self presentViewController:svc animated:YES completion:nil];
//    }
//    else{
//        SignUpViewController *svc = [[SignUpViewController alloc]initWithNibName:@"SignUpViewController" bundle:nil];
//        svc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//        [self.navigationController setNavigationBarHidden:NO animated:YES];
//        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//        [self.navigationController pushViewController:svc animated:YES];
//        
//      //  [self presentViewController:svc animated:YES completion:nil];
//    }
//    
//    
//    
//    
//}
//
//-(void)discover_clicked{
//    NSLog(@"discover");
//    
//    if(IsIphone5)
//    {
//        MainViewController *mvc = [[MainViewController alloc]initWithNibName:@"MainViewIphone5" bundle:nil];
//        [self.navigationController pushViewController:mvc animated:YES];
//    }
//    else
//    {
//    MainViewController *mvc = [[MainViewController alloc]initWithNibName:@"MainViewController" bundle:nil];
//    [self.navigationController pushViewController:mvc animated:YES];
//    }
//    
//}
//
//-(void)signin_clicked{
//    NSLog(@"sign-in");
//    
//    if (IsIphone5) {
//        SignInViewController *sivc = [[SignInViewController alloc]initWithNibName:@"SignInViewIphone5" bundle:nil];
//        sivc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//        [self.navigationController setNavigationBarHidden:NO animated:YES];
//         self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//        [self.navigationController pushViewController:sivc animated:YES];
//    }
//    else{
//        SignInViewController *sivc = [[SignInViewController alloc]initWithNibName:@"SignInViewController" bundle:nil];
//        sivc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//        [self.navigationController setNavigationBarHidden:NO animated:YES];
//        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//       [self.navigationController pushViewController:sivc animated:YES];
//    }
//    
//}


-(void) swipe{
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    
    // Setting the swipe direction.
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    
    // Adding the swipe gesture on image view
    [imageView addGestureRecognizer:swipeLeft];
    [imageView addGestureRecognizer:swipeRight];
}

- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
        NSLog(@"Left Swipe");
        
        [pageControl setCurrentPage:1];
        [UIView transitionWithView:imageView
                          duration:0.3f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            imageView.image = [UIImage imageNamed:@"foodies_bg"];
                        } completion:NULL];
    }
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
        
        [pageControl setCurrentPage:0];
        
        [UIView transitionWithView:imageView
                          duration:0.3f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            imageView.image = [UIImage imageNamed:@"intro_screen2.jpg"];
                        } completion:NULL];
        
        NSLog(@"Right Swipe");
    }
    
}


-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)discover_Imly:(id)sender {
    MainViewController *mvc = [[MainViewController alloc]initWithNibName:@"MainViewController" bundle:nil];
    [self presentViewController:mvc animated:YES completion:nil];
}

- (IBAction)signUp:(id)sender {
    
    SignUpViewController *svc =[[SignUpViewController alloc]initWithNibName:@"SignUpViewController" bundle:nil];
   [self presentViewController:svc animated:YES completion:nil];
    
    
}
- (IBAction)signIn:(id)sender {
    
    SignInViewController *svc = [[SignInViewController alloc]initWithNibName:@"SignInViewController" bundle:nil];
    [self presentViewController:svc animated:YES completion:nil];
}
@end
