//
//  AddViewController.m
//  Imly
//
//  Created by Ankur Khanna on 21/03/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import "AddViewController.h"

#import "RequestDishViewController.h"

#import "MainViewController.h"


@interface AddViewController ()
{
    UIButton *request_dish_button;
    UIButton *post_dish_button;
    AddViewController *advc;
    
    MainViewController *mvc;
}

@end

@implementation AddViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    
//    NSDictionary *viewsDic = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:request_dish_button,nil]
//                                                             forKeys:[NSArray arrayWithObjects:@"requestDish", nil]];
//    
//    [self.view  addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(<=0)-[requestDish(<=155)]-|" options:0 metrics:0 views:viewsDic]];
//    
//     [self.view  addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(<=300)-[requestDish]-(<=50)-|" options:0 metrics:0 views:viewsDic]]; //iphone 5 - <=50 .... iphone 4 - <=135
    

}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)request_dish_button_clicked:(id)sender {
    
    NSLog(@"request button pressed");
    
    RequestDishViewController *rdvc = [[RequestDishViewController alloc]initWithNibName:@"RequestDishViewController" bundle:nil];
    
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.3;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromRight;
//    [self.view.window.layer addAnimation:transition forKey:nil];
    
  //  [mvc.mainTabBar setHidden:YES];
    

    
 //   [self.navigationController pushViewController:rdvc animated:YES];
    
    
    [self presentViewController:rdvc animated:YES completion:nil];
    
}

- (IBAction)post_dish_button_clicked:(id)sender {
}
@end
