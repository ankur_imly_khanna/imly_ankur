//
//  MainViewController.m
//  Imly
//
//  Created by Ankur Khanna on 21/03/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import "MainViewController.h"



@interface MainViewController ()

@end

@implementation MainViewController

//@synthesize mainTabBar;


@synthesize tab;

@synthesize discoverViewController,favoriteViewController,addViewController,notificationViewController,profileViewController,postViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.tab=[[UITabBarController alloc]init];
    
    
   discoverViewController=[[DiscoverViewController alloc]initWithNibName:@"DiscoverViewController" bundle:nil];
    discoverViewController.title=@"Discover";
    
    favoriteViewController = [[FavoriteViewController alloc]initWithNibName:@"FavoriteViewController" bundle:nil];
    favoriteViewController.title=@"Favorite";

    postViewController = [[PostDishViewController alloc]initWithNibName:@"PostDishViewController" bundle:nil];
    UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:postViewController];
    
 
    
    notificationViewController = [[NotificationViewController alloc]initWithNibName:@"NotificationViewController" bundle:nil];
    notificationViewController.title = @"Notification";
    
    
   profileViewController = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
    profileViewController.title = @"Profile";
    
    // pvc.tabBarItem.image=[UIImage imageNamed:@"im.png"];
    
    
    
    // For Future Release
//    self.tab.viewControllers=[NSArray arrayWithObjects:discoverViewController,favoriteViewController,nc,notificationViewController,profileViewController, nil];
    
   
    
   self.tab.viewControllers=[NSArray arrayWithObjects:notificationViewController,nc,profileViewController, nil];
    
     tab.selectedIndex = 2;
    
    NSArray *tabs =  self.tab.viewControllers;
    UIViewController *tab1 = [tabs objectAtIndex:0];
    tab1.tabBarItem.image = [UIImage imageNamed:@"clockicon.png"];
    UIViewController *tab2 = [tabs objectAtIndex:1];
    tab2.tabBarItem.image = [UIImage imageNamed:@"nearest.png"];
    
    [self.view addSubview:self.tab.view];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
