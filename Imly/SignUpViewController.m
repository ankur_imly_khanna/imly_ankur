//
//  SignUpViewController.m
//  Imly
//
//  Created by Ankur Khanna on 19/03/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import "SignUpViewController.h"

#import "SignUpDetailView.h"

#import "myMacros.h"


@interface SignUpViewController ()
{
    BOOL isEmailValidationPassed;
    UIButton *next_button;
    UILabel *or_label;
    UIButton *facebook_button;

}

@end

@implementation SignUpViewController

@synthesize navBar,email_text,signUp_Dict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
        
//        self.navigationItem.hidesBackButton = YES;
//        self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc] initWithTitle:@"Put Any Title"
//                                                                                style:UIBarButtonItemStyleBordered
//                                                                               target:self
//                                                                               action:@selector(back)];
        
        
 //       navBar = [[UINavigationBar alloc]initWithFrame:CGRectMake(0,0 , self.view.frame.size.width, 66)];
 //       [navBar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
 //       [self.view addSubview:navBar];
        
//        UIButton* fakeButton = (UIButton *) [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"yourimage.png"]];
        
        
//        UIBarButtonItem *doneItem = [[UIBarButtonItem alloc]initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(back)];

        
  //      UINavigationItem *navItem = [[UINavigationItem alloc]initWithTitle:@"Sign Up"];
  //      [navItem setLeftBarButtonItem:doneItem animated:YES];
  //      [navBar setItems:[NSArray arrayWithObject:navItem] animated:YES];
        
        
        
        
//        UIImage* image3 = [UIImage imageNamed:@"mail-48_24.png"];
//        CGRect frameimg = CGRectMake(0, 0, image3.size.width, image3.size.height);
//        UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
//        [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
//        [someButton addTarget:self action:@selector(sendmail)
//             forControlEvents:UIControlEventTouchUpInside];
//        [someButton setShowsTouchWhenHighlighted:YES];
//        
//        UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
//        self.navigationItem.rightBarButtonItem=mailbutton;
        
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    signUp_Dict = [[NSMutableDictionary alloc]init];
    
    isEmailValidationPassed = NO;
    

    // Left Line of OR
    
    if(IsIphone5)
    {
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(40, 230)];
    [path addLineToPoint:CGPointMake(140, 230)];
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [path CGPath];
    shapeLayer.strokeColor = [[UIColor darkGrayColor] CGColor];
    shapeLayer.lineWidth = 1.0;
    shapeLayer.fillColor = [[UIColor clearColor] CGColor];
    [self.view.layer addSublayer:shapeLayer];
    
    
    // Right Line of OR
    
    UIBezierPath *path2 = [UIBezierPath bezierPath];
    [path2 moveToPoint:CGPointMake(280, 230.0)]; //(lenght choti, )
    [path2 addLineToPoint:CGPointMake(175.0, 230.0)]; //
    
    CAShapeLayer *shapeLayer2 = [CAShapeLayer layer];
    shapeLayer2.path = [path2 CGPath];
    shapeLayer2.strokeColor = [[UIColor darkGrayColor] CGColor];
    shapeLayer2.lineWidth = 1.0;
    shapeLayer2.fillColor = [[UIColor clearColor] CGColor];
    [self.view.layer addSublayer:shapeLayer2];
    
    }
    
    else
    {
        
        
        UIBezierPath *path = [UIBezierPath bezierPath];
        [path moveToPoint:CGPointMake(40, 200)];
        [path addLineToPoint:CGPointMake(140, 200)];
        
        CAShapeLayer *shapeLayer = [CAShapeLayer layer];
        shapeLayer.path = [path CGPath];
        shapeLayer.strokeColor = [[UIColor darkGrayColor] CGColor];
        shapeLayer.lineWidth = 1.0;
        shapeLayer.fillColor = [[UIColor clearColor] CGColor];
        [self.view.layer addSublayer:shapeLayer];
        
        
        // Right Line of OR
        
        UIBezierPath *path2 = [UIBezierPath bezierPath];
        [path2 moveToPoint:CGPointMake(280, 200.0)]; //(lenght choti, )
        [path2 addLineToPoint:CGPointMake(175.0, 200.0)]; //
        
        CAShapeLayer *shapeLayer2 = [CAShapeLayer layer];
        shapeLayer2.path = [path2 CGPath];
        shapeLayer2.strokeColor = [[UIColor darkGrayColor] CGColor];
        shapeLayer2.lineWidth = 1.0;
        shapeLayer2.fillColor = [[UIColor clearColor] CGColor];
        [self.view.layer addSublayer:shapeLayer2];
    }
    
    // Label of OR
    
    or_label = [[UILabel alloc]initWithFrame:CGRectMake(145, 210, 40, 40)];
    or_label.text = @"OR";
    [self.view addSubview:or_label];
    
  
    
    
    
//    NSDictionary *viewsDic = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:email_text,next_button,or_label, facebook_button, nil]
//                                                         forKeys:[NSArray arrayWithObjects:@"emailField",@"nextButton",@"orLabel",@"fbButton", nil]];
//    
//    [self.view  addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[emailField]-100-[nextButton]" options:0 metrics:0 views:viewsDic]];
//    
//    [self.view  addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-230-[fbButton]"      options:0 metrics:0 views:viewsDic]];
//    [self.view  addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-60-[fbButton]-60-|"      options:0 metrics:0 views:viewsDic]];
    
    
    // Do any additional setup after loading the view from its nib.
}





#pragma mark Text Field Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    textField.placeholder = nil;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    textField.placeholder = @"Enter Email Address";
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *t = [[event allTouches] anyObject];
    
    if ([email_text isFirstResponder] && [t view] != email_text) {
        [email_text resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}



#pragma mark Validate Email

- (BOOL) validateEmail: (NSString *) emailStr {
	
    
	NSString *emailvalidate = @"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}";
    
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailvalidate] ;
	
    return [emailTest evaluateWithObject:emailStr];
}

#pragma mark Dismiss View

-(void)back
{
        
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)next_button_clicked:(id)sender
{
    
    NSLog(@"Next Button Clicked");
    
    if(![email_text.text isEqual:@""])
    {
        
        if([self validateEmail:email_text.text])
        {
            isEmailValidationPassed = YES;
            
            [signUp_Dict setValue:email_text.text forKey:@"email"];
            
            NSLog(@"dict value in first page %@",signUp_Dict);
            
            
            
            SignUpDetailView *sudv = [[SignUpDetailView alloc]initWithNibName:@"SignUpDetailView" bundle:nil];
            sudv.signUp_Dict = self.signUp_Dict;
            
            self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            [self presentViewController:sudv animated:YES completion:nil];
    
            
        }
        else
        {
            UIAlertView *not_validEmail = [[UIAlertView alloc]initWithTitle:@"Invalid Email id" message:@"Enter valid Email Address" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [not_validEmail show];
        }
    }
    
    else
    {
        UIAlertView *empty_emailText = [[UIAlertView alloc]initWithTitle:@"Missing Field" message:@"Kindly fill in your Email id" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [empty_emailText show];
    }
}



- (IBAction)facebook_button_clicked:(id)sender
{
    NSLog(@"Facebook Button Clicked");
}

- (IBAction)close:(id)sender
{
    self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self dismissViewControllerAnimated:YES completion:nil];

}

-(void)viewWillAppear:(BOOL)animated{
    [self.view endEditing:YES];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

@end
