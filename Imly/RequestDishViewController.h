//
//  RequestViewController.h
//  Imly
//
//  Created by Ankur Khanna on 24/03/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestDishViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *request_dish_text;

- (IBAction)back:(id)sender;

- (IBAction)next_button_clicked:(id)sender;

@end
