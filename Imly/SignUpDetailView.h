//
//  SignUpDetailView.h
//  Imly
//
//  Created by Ankur Khanna on 20/03/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSMutableDictionary+JSONString.h"

@interface SignUpDetailView : UIViewController<UITextFieldDelegate,NSURLSessionDelegate,NSURLConnectionDelegate>

@property(nonatomic,strong) UINavigationBar *navBar;

@property (strong, nonatomic) IBOutlet UITextField *name_text;
@property (strong, nonatomic) IBOutlet UITextField *password_text;

@property (strong, nonatomic) NSMutableDictionary *signUp_Dict;

- (IBAction)signUp_button_clicked:(id)sender;

- (IBAction)close:(id)sender;

@end
