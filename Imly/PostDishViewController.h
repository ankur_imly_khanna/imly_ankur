//
//  PostDishViewController.h
//  Imly
//
//  Created by Ankur Khanna on 26/03/14.
//  Copyright (c) 2014 Ankur Khanna. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MobileCoreServices/MobileCoreServices.h>

@interface PostDishViewController : UIViewController <UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate>
{
    BOOL newMedia;
}


@property (strong, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UITextField *dish_text;
@property (weak, nonatomic) IBOutlet UILabel *center_label;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;


- (IBAction)selectPhoto:(id)sender;

- (IBAction)left_button:(id)sender;
- (IBAction)right_button:(id)sender;

@end
